// Icon for dashboard page
import Dashboard from "@material-ui/icons/Dashboard";
// core components/views for Admin layout
import DashboardPage from "views/Dashboard/Dashboard.js";

// Components authentification
import Inscription from "views/Index/Inscription.js";
import Confirmation from "views/Index/Confirmation.js";
import Connection from "views/Index/Connection";

//Components center
import Actualite from "components/Diariko/Accueil/SectionCenter";
import Analyse from "components/Diariko/Analyse/Analyse.js";
import Famille from "components/Diariko/Famille/Famille.js";
import Resultat from "components/Diariko/Analyse/Resultat";
import Conseil from "components/Diariko/Conseil/Covid19";
import Detail from "components/Diariko/Conseil/Detail";
import Detail2 from "components/Diariko/Conseil/Detail2";
import Events from "components/Diariko/Events/Events";
import Job from "components/Diariko/Job/Job";


//Components profil
import Profil from "components/Diariko/Profil/Profil";

import AnalyseApiMedic from "components/Diariko/Analyse/AnalyseApiMedic.js";

// Components accueil
import Accueil from "views/Index/Accueil.js";

let dashboardRoutes = [
  {
    path: "/",
    name: "Dashboard",
    rtlName: "لوحة القيادة",
    icon: Dashboard,
    component: DashboardPage,
    layout: "/accueil",
  },
  {
    path: "/inscription",
    name: "Inscription",
    rtlName: "Inscription",
    icon: Dashboard,
    component: Inscription,
    layout: "/accueil",
  },
  {
    path: "/confirmation",
    name: "Confirmation",
    rtlName: "Confirmation",
    icon: Dashboard,
    component: Confirmation,
    layout: "/accueil",
  },
  {
    path: "/",
    name: "Connection",
    rtlName: "Connection",
    icon: Dashboard,
    component: Connection,
    layout: "/connection",
  },
  {
    path: "/diariko",
    name: "Accueil",
    rtlName: "Accueil",
    icon: Dashboard,
    component: Accueil,
    layout: "/index",
  },
  {
    path: "/analyse",
    name: "Analyse",
    rtlName: "Accueil",
    icon: Dashboard,
    component: Analyse,
    layout: "/index",
  },
  {
    path: "/actualite",
    name: "Actualite",
    rtlName: "Actualite",
    icon: Dashboard,
    component: Actualite,
    layout: "/index",
  },
  {
    path: "/famille",
    name: "Famille",
    rtlName: "Famille",
    icon: Dashboard,
    component: Famille,
    layout: "/index",
  },
  {
    path: "/analyseApiMedic",
    name: "Analyse",
    rtlName: "Analyse",
    icon: Dashboard,
    component: AnalyseApiMedic,
    layout: "/index",
  },
  {
    path: "/resultat",
    name: "Resultat de diagnostic",
    rtlName: "Resultat de diagnostic",
    icon: Dashboard,
    component: Resultat,
    layout: "/index",
  },
  {
    path: "/profil",
    name: "Mon profil",
    rtlName: "Mon profil",
    icon: Dashboard,
    component: Profil,
    layout: "/index",
  },
  {
    path: "/conseil",
    name: "Conseil",
    rtlName: "Coseil",
    icon: Dashboard,
    component: Conseil,
    layout: "/index",
  },
  {
    path: "/detail1",
    name: "Detail",
    rtlName: "Detail",
    icon: Dashboard,
    component: Detail,
    layout: "/index",
  },
  {
    path: "/detail2",
    name: "Detail 2",
    rtlName: "Detail 2",
    icon: Dashboard,
    component: Detail2,
    layout: "/index",
  },
  {
    path: "/events",
    name: "Events",
    rtlName: "Events",
    icon: Dashboard,
    component: Events,
    layout: "/index",
  },
  {
    path: "/job",
    name: "Job",
    rtlName: "Job",
    icon: Dashboard,
    component: Job,
    layout: "/index",
  }
]
export default dashboardRoutes;
