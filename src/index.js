/*!

=========================================================
* Material Dashboard React - v1.9.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect } from "react-router-dom";

// core components
import Admin from "layouts/Admin.js";
import AdminConnection from "layouts/AdminConnection.js";
import Racing from "layouts/Racing.js";
//import Profil from "layouts/ProfilLayout.js";
//import Conseil from "layouts/ConseilLayout.js";

import "assets/css/material-dashboard-react.css?v=1.9.0";
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import "assets/css/config.scss";

import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';


// service worker
//import * as serviceWorker from "./serviceWorker";

library.add(fab);

const hist = createBrowserHistory();
ReactDOM.render(
  <Router history={hist}>
    <Switch>
      <Route path="/accueil" component={Admin} />
      <Route path="/index" component={Racing} />
      <Route path="/connection" component={AdminConnection} />
      <Redirect from="/" to="/accueil" />
    </Switch>
  </Router>,
  document.getElementById("root")
)