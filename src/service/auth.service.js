import axios from "axios";

//const API_URL = "http://localhost:8080/api/auth/";

const API_URL = "https://diariko.com/api/auth/";

class AuthService {
	login(username, password) {
		return axios.post(API_URL + "signin", {
				username,
				password
			})
			.then(response => {
				if (response.data.accessToken) {
					localStorage.setItem("user", JSON.stringify(response.data));
				}
				return response.data;
			});
	}

	logout() {
		localStorage.removeItem("user");
		console.log("User deconnected");
	}

	register(nom, prenom, dateNais, adresse, email, password, username, pays, region, commune, tel) {
		return axios.post(API_URL + "signup", {
			nom, 
			prenom, 
			dateNais, 
			adresse, 
			email, 
			password, 
			username, 
			pays, 
			region, 
			commune, 
			tel
		});
	}

	getCurrentUser() {
		if ( localStorage.getItem('user') !== null)
		{
			return JSON.parse(localStorage.getItem('user'));
		}
		else {
			console.log("Local storage error");
		}
	}

	findOne(id) {
		axios.get(API_URL + "findOne/" + id).then(user => {
			return user.data;
		});
	}
}

export default new AuthService();