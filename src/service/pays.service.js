import axios from "axios";

//const API_URL = "http://localhost:8080/api/pays";

const API_URL = "https://diariko.com/api/pays";
class PaysService {
	getPays() {
		axios.get(API_URL + "")
			.then(response => {
				console.log(response.data);
				return response.data;
			});
	}

	getRegion(paysId) {
		const id = paysId;
		return axios.get(API_URL + "/region/" + id)
			.then(response => {
				return response.data;
			});
	}

	getCommune(regionId) {
		const id = regionId;
		return axios.get(API_URL + "/commune/" + id)
			.then(response => {
				return response.data;
			});
	}
	findPays(id) {
		let pays = [];
		axios.get(API_URL + "/" + id)
			.then(response => {
				pays = response.data;
			});
		return pays;
	}
	
}

export default new PaysService();