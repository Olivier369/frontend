import React from "react";
//import { withRouter } from "react-router-dom";

//import axios from "axios";

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { Form, FormGroup } from 'reactstrap';

import "assets/css/diariko.css";
import "assets/css/center.scss";

import logo from "assets/img/font/logo.png";
//import Img2 from "assets/img/sidebar-4.jpg";
//import Avatar from '@material-ui/core/Avatar';
import Search from '@material-ui/icons/Search';
import Logout from '@material-ui/icons/ExitToApp';

import IconButton from '@material-ui/core/IconButton';

//Badge
import Badge from '@material-ui/core/Badge';
import MailIcon from '@material-ui/icons/Mail';
import NotificationsIcon from '@material-ui/icons/Notifications';

import AuthService from "service/auth.service";

class Navbar extends React.Component {
  constructor(props) {
    super(props);
    this.logout = this.logout.bind(this);
    this.state = {
      loading: false
    }

  }

  logout() {
    this.setState({
      loading: true
    });

    AuthService.logout();

    this.setState({
      loading: false
    });

    const { history } = this.props;
    history.push("/");

  }

  render(){
    return (
      <AppBar position="fixed" id="nav-font-acc">
        <Toolbar>
            <div className="logo" style={{ backgroundImage: `url(${require("assets/img/font/logo.png")})` }}></div>
            <div className="form-connection">
              <Form id="form-input">
                <FormGroup id="form-recherche">
                  <IconButton aria-label="Search">
                    <Search />
                  </IconButton>
                  <IconButton aria-label="Mails">
                    <Badge  color="secondary">
                      <MailIcon />
                    </Badge>
                  </IconButton>
                  <IconButton aria-label="Notifications">
                    <Badge  color="secondary">
                      <NotificationsIcon />
                    </Badge>
                  </IconButton>
                  <IconButton onClick={ this.logout } aria-label="Deconnecter">
                    {this.state.loading && (
                      <span className="spinner-border spinner-border-sm"></span>
                    )}
                    
                    <Badge  color="secondary">
                      <Logout />
                    </Badge>
                  </IconButton>
                </FormGroup>
              </Form>
            </div>       
        </Toolbar>
      </AppBar>
    );
  }
}
export default Navbar;