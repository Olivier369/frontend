import React from "react";
import { Link } from 'react-router-dom';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { Form, FormGroup } from 'reactstrap';

import "assets/css/diariko.css";
import logo from "assets/img/font/logo.png";

import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  button: {
    margin: theme.spacing(1),
  },
}));

export default function Header(props) {
  const classes = useStyles();
  return (
    <AppBar position="fixed" id="nav-font">
      <Toolbar>
          <div className="logo" style={{ backgroundImage: `url(${require("assets/img/font/logo.png")})` }}></div>
          <div className="form-connection">
            <Form id="form-input">
              <FormGroup id="form-btn-conx">
                <Link to="/connection">
                  <Button
                    className={classes.button}
                    id="btnDiariko"
                  >
                    Se connecter
                  </Button>
                </Link>
                </FormGroup>
            </Form>
          </div>       
      </Toolbar>
    </AppBar>
  );
}
