import React from "react";
import {
  CardText,
  CardSubtitle,
  Row, Col, Container
} from 'reactstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome, faCoffee, faAd } from "@fortawesome/free-solid-svg-icons";

import "assets/css/diariko.css";



export default function Activite() {
  return (
    <Container className="themed-container fix-cont cont-act" fluid={true}>
    	<div className="head">
    		<p className="titreAct">Ce que nous vous offrons</p>
    	</div>
    	<Row id="row-fuild">
    		<Col xs="6" sm="3" id="act-col">
    			<div className="act_icon">
    				<FontAwesomeIcon icon={faHome} size="2x"/>
    			</div>
    			<div className="act_body center">
			        <CardSubtitle tag="h6" className="mb-2 text-muteds">Emploi, Stage</CardSubtitle>
			        <CardText className="cont_p">
		          		Mettre en ligne CV, jobs expériences et formation 
		          		pour que les recruteurs puissent vous contacter rapidement. 
		          	</CardText>
    			</div>
    		</Col>
    		<Col xs="6" sm="3" id="act-col">
    			<div className="act_icon">
    				<FontAwesomeIcon icon={faCoffee} size="2x"/>
    			</div>
    			<div className="act_body center">
			        <CardSubtitle tag="h6" className="mb-2 text-muteds">Data storage</CardSubtitle>
			        <CardText className="cont_p">
		          		Pour assister dans la sécurisation de vos informations et données, noter ici les données importantes de vos appareils
		          	</CardText>
    			</div>
    		</Col>
    		<Col xs="6" sm="3" id="act-col">
    			<div className="act_icon">
    				<FontAwesomeIcon icon={faAd} size="2x"/>
    			</div>
    			<div className="act_body center">
			        <CardSubtitle tag="h6" className="mb-2 text-muteds">Formation</CardSubtitle>
			        <CardText className="cont_p">
		          		On vous propose des différents types de formations que ce soit professionnelles ou académiques. 
			            Ces formations sont assurées par des formateurs expérimentés et professionnels
		          	</CardText>
    			</div>
    		</Col>
    	</Row>

    </Container>	
    
  );
}