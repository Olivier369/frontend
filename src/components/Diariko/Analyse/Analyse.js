import React from "react";
import { useHistory } from "react-router-dom";

import { makeStyles, withStyles, useTheme } from "@material-ui/core/styles";
//import { Input } from "reactstrap";

import Button from '@material-ui/core/Button';

//modal component
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

// checkbox importation
import { green } from '@material-ui/core/colors';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

// stepper components
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

//Radio button
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

// Select multiple
import Chip from '@material-ui/core/Chip';
import Input from '@material-ui/core/Input';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

import "assets/css/diariko.css";
import "assets/css/center.scss";

const useStyles = makeStyles((theme) => ({
  button: {
    margin: theme.spacing(1),
  },

  actionsContainer: {
    marginBottom: theme.spacing(2),
  },
  resetContainer: {
    padding: theme.spacing(3),
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 2,
  },
  noLabel: {
    marginTop: theme.spacing(3),
  },
}));

//Checkbox style

const GreenCheckbox = withStyles({
  root: {
    color: green[400],
    '&$checked': {
      color: green[600],
    },
  },
  checked: {},
})((props) => <Checkbox color="default" {...props} />);

// Multi select style
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const names = [
  'Irritabilité inhabituelle',
  'Selles liquides',
  'Vomissements',
  'Douleurs abdominales',
  'Cyanose',
  'Prise de boisson impossible',
  'Allaitement impossible',
  'Perte de connaissance',
  'Perte convulsions',
  'Tachycardie',
  'Marbrures',
  'Douleurs dans la poitrine',
  'Faiblesse générale',
  'Sensation de malaise',
  'Douleurs musculaires',
];

function getStyles(name, personName, theme) {
  return {
    fontWeight:
      personName.indexOf(name) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
  };
}

export default function Analyse() {
	const history = useHistory();

	const routeChange = () => {
	    let path = `/index/resultat`;
	    history.push(path);
	};

	const classes = useStyles();
	const [open, setOpen] = React.useState(false);

	const handleClickOpen = () => {
      setOpen(true);
    };

	const handleClose = () => {
      setOpen(false);
    };

  // Checked bouton
	const [state, setState] = React.useState({
	    checkedA: false,
	    checkedB: false,
	    checkedC: false,
	    checkedD: false,
	    checkedE: false,
	    checkedF: false,
	    checkedG: false,
	    checkedH: false,
	    checkedI: false,
	});

	const handleChangeCheckbox = (event) => {
	    setState({ ...state, [event.target.name]: event.target.checked });
	};

	// stepper
	// 
	const [activeStep, setActiveStep] = React.useState(0);

	const handleNext = () => {
	    setActiveStep((prevActiveStep) => prevActiveStep + 1);
	  };

	const handleBack = () => {
	    setActiveStep((prevActiveStep) => prevActiveStep - 1);
	  };

	const handleReset = () => {
	    setActiveStep(0);
	  };

	// value of radio bouton
    const [value, setValue] = React.useState('femelle');

	const handleChangeRadio = (event) => {
	    setValue(event.target.value);
	};

	// Value fo select multiple
	const theme = useTheme();
	const [personName, setPersonName] = React.useState([]);

	const handleChangeSelectMultiple = (event) => {
	    setPersonName(event.target.value);
	 };

	 // Router link

	return (
		<div>
			<div className="boxShadow mb-10">
				<div className="actMsg">
					<h5 className="colorDiariko">COVID-19</h5>
					<p>La COVID-19 affecte les individus de différentes manières. La plupart des personnes infectées développent une forme légère à modérée de la maladie et guérissent sans hospitalisation</p>
					<p>Effectuer vos diagnostic en restant connecte</p>
					<div className="flex-center">
						<Button variant="outlined" color="primary" size="medium" disabled className={classes.button} onClick={handleClickOpen}>Commencer</Button>
					</div>
					<Dialog open={open} aria-labelledby="form-dialog-title" id="dialogForm">

				        <DialogTitle id=""className="colorDiariko">Diagnostic rapid sur la pandemie COVID-19</DialogTitle>

				        <DialogContent>
				          <div>

				          	<Stepper activeStep={activeStep} orientation="vertical">
				          		  <Step key={0}>
						            <StepLabel>Information du patient</StepLabel>
						            <StepContent>
						              <Typography>
						              	<div>
							          		<FormControl component="fieldset">
										      <RadioGroup aria-label="gender" name="gender1" value={value} onChange={handleChangeRadio}>
										      	<FormControlLabel id="fs-13" value="mal" control={<Radio />} label="Mal" />
										        <FormControlLabel id="fs-13" value="femelle" control={<Radio />} label="Femelle" />
										        
										      </RadioGroup>
										    </FormControl>
										    <br/>
										    <FormControl component="fieldset">
										      <FormLabel component="legend" id="fs-13">Age</FormLabel>
										      <Input type="number" id="age"></Input>
										    </FormControl>
										    <br/>
							          	</div>
						              </Typography>
						              <div className={classes.actionsContainer}>
						                <div>

						                  <Button variant="contained" color="primary" onClick={handleNext} className={classes.button}>
						                    Suivant
						                  </Button>
						                </div>
						              </div>
						            </StepContent>
						          </Step>
						          <Step key={1}>
						            <StepLabel>Symptômes les plus fréquents</StepLabel>
						            <StepContent>
						              <Typography>
						              	<div>
							          		<FormControlLabel
										        control={<GreenCheckbox checked={state.checkedA} onChange={handleChangeCheckbox} name="checkedA" />}
										        label="Fievre. ( à partir de 37,8 °C)" className="checkBox"
										      />
										    <br/>
										    <FormControlLabel
										        control={<GreenCheckbox checked={state.checkedB} onChange={handleChangeCheckbox} name="checkedB" />}
										        label="Toux persistante (généralement sèche)" className="checkBox"
										      />
										     <br/>
										    <FormControlLabel
										        control={<GreenCheckbox checked={state.checkedC} onChange={handleChangeCheckbox} name="checkedC" />}
										        label="Fatigues" className="checkBox"
								      		/>
								      		<br/>
										    <FormControlLabel
										        control={<GreenCheckbox checked={state.checkedC} onChange={handleChangeCheckbox} name="checkedC" />}
										        label="Une perte du goût et/ou de l'odorat" className="checkBox"
								      		/>
				          				</div>
						              </Typography>
						              <div className={classes.actionsContainer}>
						                <div>
						                  <Button disabled={activeStep === 0} onClick={handleBack} className={classes.button} >Retour</Button>
						                  
						                  <Button variant="contained" color="primary" onClick={handleNext} className={classes.button}>
						                    Suivant
						                  </Button>
						                </div>
						              </div>
						            </StepContent>
						          </Step>
						          <Step key={2}>
						            <StepLabel>Symptômes moins fréquents</StepLabel>
						            <StepContent>
						              <Typography>
						              	<div>
							          		<FormControlLabel
										        control={<Checkbox checked={state.checkedD} onChange={handleChangeCheckbox} name="checkedD" />}
										        label="Courbatures" className="checkBox"
										      />
										    <br/>
										    <FormControlLabel
										        control={<Checkbox checked={state.checkedE} onChange={handleChangeCheckbox} name="checkedE" />}
										        label="Maux de gorges" className="checkBox"
										      />
										    <br/>
										    <FormControlLabel
										        control={<Checkbox checked={state.checkedF} onChange={handleChangeCheckbox} name="checkedF" />}
										        label="Diarree" className="checkBox"
										      />
										    <br/>
										    <FormControlLabel
										        control={<Checkbox checked={state.checkedG} onChange={handleChangeCheckbox} name="checkedG" />}
										        label="Conjonctive" className="checkBox"
										     />
										    <br/>
										    <FormControlLabel
										        control={<Checkbox checked={state.checkedH} onChange={handleChangeCheckbox} name="checkedH" />}
										        label="Un nez qui coule ou bouché." className="checkBox"
										      />
										    <br/>
										    
										    <FormControlLabel
										        control={<Checkbox checked={state.checkedH} onChange={handleChangeCheckbox} name="checkedH" />}
										        label="Des maux de tête." className="checkBox"
										      />
										      <br/>
										    <FormControlLabel
										        control={<Checkbox checked={state.checkedI} onChange={handleChangeCheckbox} name="checkedI" />}
										        label="Eruption cutanee ou decoloration des doigts ou des orteils" className="checkBox"
										      />
							          	</div>
						              </Typography>
						              <div className={classes.actionsContainer}>
						                <div>
						                  <Button disabled={activeStep === 0} onClick={handleBack} className={classes.button} >Retour</Button>
						                  <Button variant="contained" color="primary" onClick={handleNext} className={classes.button}>
						                    Suivant
						                  </Button>
						                </div>
						              </div>
						            </StepContent>
						          </Step>
						          <Step key={3}>
						            <StepLabel>Symptômes graves</StepLabel>
						            <StepContent>
						              <Typography>
						              	<div>
							          		<FormControlLabel
										        control={<Checkbox checked={state.checkedD} onChange={handleChangeCheckbox} name="checkedD" />}
										        label="Dificulte a la respiration ou essouflement" className="checkBox"
										      />
										    <FormControlLabel
										        control={<Checkbox checked={state.checkedE} onChange={handleChangeCheckbox} name="checkedE" />}
										        label="Sesnation d'oppression ou douleur au niveau de la poitrine" className="checkBox"
										      />
										    <FormControlLabel
										        control={<Checkbox checked={state.checkedF} onChange={handleChangeCheckbox} name="checkedF" />}
										        label="Perte d'elocution ou de motricite" className="checkBox"
										      />
							          	</div>
						              </Typography>
						              <div className={classes.actionsContainer}>
						                <div>
						                  <Button disabled={activeStep === 0} onClick={handleBack} className={classes.button} >Retour</Button>
						                  <Button variant="contained" color="primary" onClick={handleNext} className={classes.button}>
						                    Suivant
						                  </Button>
						                </div>
						              </div>
						            </StepContent>
						          </Step>
						          <Step key={4}>
						            <StepLabel>Autres</StepLabel>
						            <StepContent>
						              <Typography>
						              	<div>
							          		<FormControl className={classes.formControl}>
										        <p id="fs-13">Quoi d'autres symptomes avez-vous?</p>
										        <Select
										          labelId="demo-mutiple-chip-label"
										          id="demo-mutiple-chip"
										          multiple
										          value={personName}
										          onChange={handleChangeSelectMultiple}
										          input={<Input id="select-multiple-chip" />}
										          renderValue={(selected) => (
										            <div className={classes.chips}>
										              {selected.map((value) => (
										                <Chip key={value} label={value} className={classes.chip} />
										              ))}
										            </div>
										          )}
										          MenuProps={MenuProps}
										        >
										          {names.map((name) => (
										            <MenuItem key={name} value={name} style={getStyles(name, personName, theme)}>
										              {name}
										            </MenuItem>
										          ))}
										        </Select>
										      </FormControl>
							          	</div>
						              </Typography>
						              <div className={classes.actionsContainer}>
						                <div>
						                  <Button disabled={activeStep === 0} onClick={handleBack} className={classes.button} >Retour</Button>
						                  <Button variant="contained" color="primary" onClick={handleNext} className={classes.button}>
						                    {activeStep === 4 ? 'Terminer' : 'Suivant'}
						                  </Button>
						                </div>
						              </div>
						            </StepContent>
						          </Step>
						      </Stepper>

						      {activeStep === 5 && (
						        <Paper square elevation={0} className={classes.resetContainer}>
						          <p id="fs-13">Vous avez terminer le diagnostic.</p>
						          <Button onClick={handleReset} className={classes.button}>
						            Recommencer
						          </Button>
						          <Button onClick={routeChange} color="primary">
						            Analyser
						          </Button>
						        </Paper>
						      )}

				          </div>
				        </DialogContent>

				        <DialogActions>
				          	<Button onClick={handleClose} color="primary">
					            Annuler
					        </Button>
				        </DialogActions>
				    </Dialog>
					<br/>
					<div>
						<h6 className="colorDiariko">Vos dernier resultat d'analyse</h6>
						<p>Vous n'avez pas encore effectuer aucun analyse.</p>
						<p>Contacter ou envoyer vos donnes medical vers vos medecins professionneles si vous avez remarquez que votre sante n'est pas au top</p>
					</div>
				</div>
				<br/>
				
			</div>
		</div>	
	);						      
}
