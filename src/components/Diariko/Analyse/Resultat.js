import React from 'react';
//import { Row, Col } from "reactstrap";
//modal component
import "assets/css/diariko.css";
import "assets/css/center.scss";

import Paper from '@material-ui/core/Paper';
import {
  Chart,
  BarSeries,
  Title,
  ArgumentAxis,
  ValueAxis,
} from '@devexpress/dx-react-chart-material-ui';

import { Animation } from '@devexpress/dx-react-chart';

const data = [
  { year: 'Rhume', population: 34 },
  { year: 'COVID-19', population: 53 },
  { year: 'Fatigue', population: 10 },
  { year: 'Deshydratation', population: 86 },
];



class Resultat extends React.Component {
	constructor(props) {
	    super(props);

	    this.state = {
	      data,
	    };
	}
	render() {
		const { data: chartData } = this.state;
		return (
			<div>
	          <div>
	          	<p className="colorDiariko">Resultat du votre test de diagnostic COVID-19</p>
	          	<div className="information block">
	          		<div className="flex w100">
	          			<label htmlFor="date" className="align lab150">Date</label><p className="align" id="date">12 decembre 2019</p> 
	          		</div>
	          		<div className="flex w100">
	          			<label htmlFor="age" className="align lab150">Age</label><p className="align" id="date">23</p> 
	          		</div>
	          		<div className="flex w100">
	          			<label htmlFor="sexe" className="align lab150">Sexe</label><p className="align" id="date">Male</p> 
	          		</div>
	          	</div>
	          	<div className="symptom">
	          		<div className="flex w100">
	          			<label htmlFor="symptom" className="align lab150">Symptome</label>
	          			<p className="align flex wrap" id="symptome">
	          				<h6>Maut de tete</h6>
	          				<h6>Toux seche</h6>
	          				<h6>Fievre grave</h6>
	          				<h6>Douleurs musculaires</h6>
	          				<h6>Migraine</h6>
	          				<h6>Detresse respiratoires</h6>
	          				<h6>Infection</h6>
	          			</p> 
	          		</div>
	          	</div>
	          	<div className="estimation">
	          		<div className="flex w100">
	          			<label htmlFor="estimate" className="align lab150 colorDiariko">Estimation</label>
	          			<div className="align mb-10" id="estimate">
	          				<div>
	          					<Paper>
							        <Chart
							          data={chartData}
							          id="chart"
							        >
							          <ArgumentAxis />
							          <ValueAxis max={100} />

							          <BarSeries
							            valueField="population"
							            argumentField="year"
							          />
							          <Title text="Resultat entre 0 à 100" />
							          <Animation />
							        </Chart>
							    </Paper>
	          				</div>
	          				<br/>
	          				<p>
	          					D'apres les informations que vous avez repondez, votre taux de risque en infection du 
	          					<span className="colorDiariko"> COVID-19</span> est de <strong className="colorDiariko">54 %</strong>.
	          				</p>
	          				<p>
	          					Vous devez consulter immediatement un medecin pour effectuer un test.
	          				</p>
	          			</div> 
	          		</div>
	          	</div>
	          </div>
			</div>
		);
	}
}
export default Resultat;