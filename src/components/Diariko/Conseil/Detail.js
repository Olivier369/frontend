// Section center, actualite,...
import React from "react";
// @material-ui/core
//import { makeStyles } from "@material-ui/core/styles";

import Avatar from '@material-ui/core/Avatar';

import MoreVertIcon from '@material-ui/icons/MoreVert';
import IconButton from '@material-ui/core/IconButton';
import FavoriteIcon from '@material-ui/icons/Favorite';
//import Send from '@material-ui/icons/Send';

// core components
import Couverture from "assets/img/profil/coverture2.jpg";

//import TextField from '@material-ui/core/TextField';

import "assets/css/diariko.css";
import "assets/css/center.scss";

export default function Center() {
//	const classes = useStyles();

  return (
  	<div>
	  	<div className="boxShadow mb-10">
	    	<div className="sectionCenter">

	            <div className="actTete flex w100">
	            	<div className="pdpRonde">
	            		<Avatar alt="Travis Howard" src={ Couverture } />
	            	</div>
	            	<div className="nomPdp nom50">
	            		<span className="colorDiariko spanN">Team DIARIKO</span>
	            		<br/>
	            		<span className="spanD">23 Decembre, 2020</span>
	            	</div>
	            	<div className="w100">
	            		<IconButton className="right" aria-label="more"><MoreVertIcon /></IconButton>
	            	</div>
	            </div>

	            <div className="actMsg">
	            	<h5 className="colorDiariko">
	            		Restez chez vous. Sauvez des vies.
	            	</h5>
	            	<p>
	            		Protégez-vous et protégez les autres en vous tenant informé et en prenant les précautions 
	            		appropriées. Suivez les conseils des autorités sanitaires locales.
	            	</p>
	            	<p>
	            		Pour empêcher la propagation de la COVID-19, suivez ces recommandations :
						Lavez-vous fréquemment les mains. Utilisez du savon et de l'eau, ou une solution hydroalcoolique.
						Tenez-vous à distance de toute personne qui tousse ou éternue.
						Portez un masque lorsque la distanciation physique n'est pas possible.
						Évitez de vous toucher les yeux, le nez ou la bouche.
						En cas de toux ou d'éternuement, couvrez-vous le nez et la bouche avec le pli du coude ou avec un mouchoir.
						Restez chez vous si vous ne vous sentez pas bien.
						Consultez un professionnel de santé si vous avez de la fièvre, que vous toussez et que vous avez des difficultés à respirer.
	            	</p>	
	            	<p>
	            		Prévenez le professionnel de santé par téléphone au préalable. Il pourra ainsi vous orienter rapidement vers l'établissement 
	            		de santé adéquat. Cela vous protège, et empêche la propagation des virus et d'autres infections.
	            	</p>
	            	<p>
	            		Les masques peuvent contribuer à éviter que les personnes qui les portent transmettent le virus à d'autres personnes. 
	            		Le seul port du masque ne protège pas contre la COVID-19. Il doit être associé à des mesures 
	            		de distanciation physique et d'hygiène des mains. Suivez les conseils des autorités sanitaires locales.
	            	</p>
	            </div>

	            <div className="actCoeur">
	            	<div className="flex w100">
		            	<div className="flex1">
			            	<IconButton aria-label="add to favorites"><FavoriteIcon /></IconButton>
					        	<span className="spanD">5 J'adore</span>
				        </div>
				        <div className="flexR">
				        	<span className="spanD right">0 Commentaires</span>
				        </div>
			        </div>
	            </div>   
	        </div>
	    </div>
    </div>
  );
}
