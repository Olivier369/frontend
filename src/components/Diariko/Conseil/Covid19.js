// Section center, actualite,...
import React from "react";
// @material-ui/core
import { makeStyles } from "@material-ui/core/styles";
import { Link, useHistory } from "react-router-dom";

import Avatar from '@material-ui/core/Avatar';


import Button from '@material-ui/core/Button';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import IconButton from '@material-ui/core/IconButton';
//import FavoriteIcon from '@material-ui/icons/Favorite';
//import Send from '@material-ui/icons/Send';

// core components
//import Profil from "assets/img/sidebar-2.jpg";
//import Img1 from "assets/img/sidebar-3.jpg";
//import Img2 from "assets/img/sidebar-4.jpg";
//import Img3 from "assets/img/sidebar-1.jpg";

import Couverture from "assets/img/profil/coverture2.jpg";


import TextField from '@material-ui/core/TextField';

import "assets/css/diariko.css";
import "assets/css/center.scss";

export default function Center() {
//  	const classes = useStyles();

  	const history = useHistory();

  return (
  	<div>
  		<div className="boxShadow mb-10 fixed" id="paddAuto">
	    	<div className="sectionCenter">
	            <div className="actTete flex w100">
	            	<div className="pdpRonde">
	            		<Avatar alt="Travis Howard" src={ Couverture } />
	            	</div>
	            	<div className="w100 ml-10">
	            		<TextField
					          id="filled-textareas"
					          label="Partager vos conseils..."
					          placeholder=""
					          multiline
					          variant="filled"
					          className="w100 field"
					          
					        />
					        
	            	</div>
	            	<Button disabled id="btnSimple">
	            		<span> Partager</span>
	            	</Button>
	            </div>
	        </div>
	    </div>
	  	<div className="boxShadow mb-10">
	    	<div className="sectionCenter">
	            <div className="actTete flex w100">
	            	<div className="pdpRonde">
	            		<Avatar alt="Travis Howard" src={ Couverture } />
	            	</div>
	            	<div className="nomPdp nom50">
	            		<span className="colorDiariko spanN">Team DIARIKO</span>
	            		<br/>
	            		<span className="spanD">23 Decembre, 2020</span>
	            	</div>
	            	<div className="w100">
	            		<IconButton className="right" aria-label="more"><MoreVertIcon /></IconButton>
	            	</div>
	            </div>
	            <div className="actMsg">
	            	<h5 className="colorDiariko">
	            		Restez chez vous. Sauvez des vies.
	            	</h5>
	            	<p>
	            		Protégez-vous et protégez les autres en vous tenant informé et en prenant les précautions 
	            		appropriées. Suivez les conseils des autorités sanitaires locales.
	            		<Link to="/index/detail1">
	            			<span className="cursor colorDiariko lire"> Afficher la suite...</span>
	            		</Link>
	            	</p>
	            </div>    
	        </div>
	    </div>
	    <div className="boxShadow mb-10">
	    	<div className="sectionCenter">
	            <div className="actTete flex w100">
	            	<div className="pdpRonde">
	            		<Avatar alt="Travis Howard" src={ Couverture } />
	            	</div>
	            	<div className="nomPdp nom50">
	            		<span className="colorDiariko spanN">Team DIARIKO</span>
	            		<br/>
	            		<span className="spanD">23 Decembre, 2020</span>
	            	</div>
	            	<div className="w100">
	            		<IconButton className="right" aria-label="more"><MoreVertIcon /></IconButton>
	            	</div>
	            </div>
	            <div className="actMsg">
	            	<h5 className="colorDiariko">
	            		Les questions les plus posées dans un entretien d'ambauche
	            	</h5>
	            	<p>
	            		Parlez-moi de vous?
						Soyez synthétique et clair, réponse en 30 secondes max. Recentrez la question sur votre parcours professionnel. 
						Commencez par vos études, votre premier poste, le second, etc. jusqu à votre situation d'aujourd'hui.
	            		<Link to="/index/detail2">
	            			<span className="cursor colorDiariko lire"> Afficher la suite...</span>
	            		</Link>
	            	</p>
	            </div>    
	        </div>
	    </div>
    </div>
  );
}
