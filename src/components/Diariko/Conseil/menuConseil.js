import React from "react";
import { Link } from "react-router-dom";
// @material-ui/core
import { makeStyles } from "@material-ui/core/styles";
import "assets/css/diariko.css";
import avatar from 'assets/img/sidebar-1.jpg';

// @material-ui/icons
import Button from '@material-ui/core/Button';
 
import Language from '@material-ui/icons/Language';
//import Money from '@material-ui/icons/AttachMoney';
//import Event from '@material-ui/icons/EventAvailable';
//import Conseil from '@material-ui/icons/WorkOutline';
//import Work from '@material-ui/icons/BusinessCenter';
//import Famille from '@material-ui/icons/PeopleOutline';
import AccountBox from '@material-ui/icons/AccountBox';

//dropdown menu
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Fade from '@material-ui/core/Fade';

const useStyles = makeStyles((theme) => ({
  button: {

  },
}));



export default function MenuDropdown() {
  const classes = useStyles();

  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <div>
        <div className="">
            <div className="profilImg pt-15">
                <div className="avatar flex-center">
                    <img src={ avatar } className="imgProfil" alt="avatar"/>
                </div>
                <div className="nomProfil">
                    <p>Olivier<br/>RATSIMBAZAFY</p>
                </div>
            </div>
            <div className="bar">

            </div> 
        	<div className="menu flex-center">
                <ul id="menuSimple" className="pl-0">
                    <li> 
                        <Link to="/index/actualite">
                            <Button variant="contained" color="primary"  size="medium" id="btnSimple" className={classes.button} startIcon={<Language/>}>Actualite</Button>
                        </Link>
                    </li>
                    <li> 
                        <Button variant="contained" aria-controls="fade-menu" aria-haspopup="true" onMouseOver={handleClick} color="primary"  size="medium" id="btnSimple" className={classes.button} startIcon={<AccountBox/>}>Conseil</Button>
                        <Menu id="fade-menu" anchorEl={anchorEl} keepMounted open={open} onClose={handleClose} TransitionComponent={Fade} >
                            <MenuItem onClick={handleClose}>COVID-19</MenuItem>
                            <MenuItem onClick={handleClose}>Bien etre</MenuItem>
                            <MenuItem onClick={handleClose}>Alimentation</MenuItem>
                            <MenuItem onClick={handleClose}>Sport</MenuItem>
                        </Menu>
                    </li>                
                </ul>
            </div>
        </div>
    </div>
  );
}
