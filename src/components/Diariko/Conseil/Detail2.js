// Section center, actualite,...
import React from "react";
// @material-ui/core
//import { makeStyles } from "@material-ui/core/styles";

import Avatar from '@material-ui/core/Avatar';

import MoreVertIcon from '@material-ui/icons/MoreVert';
import IconButton from '@material-ui/core/IconButton';
import FavoriteIcon from '@material-ui/icons/Favorite';
//import Send from '@material-ui/icons/Send';

// core components
import Couverture from "assets/img/profil/coverture2.jpg";

//import TextField from '@material-ui/core/TextField';

import "assets/css/diariko.css";
import "assets/css/center.scss";


export default function Center() {
//	const classes = useStyles();

  return (
  	<div>
	  	<div className="boxShadow mb-10">
	    	<div className="sectionCenter">

	            <div className="actTete flex w100">
	            	<div className="pdpRonde">
	            		<Avatar alt="Travis Howard" src={ Couverture } />
	            	</div>
	            	<div className="nomPdp nom50">
	            		<span className="colorDiariko spanN">Team DIARIKO</span>
	            		<br/>
	            		<span className="spanD">23 Decembre, 2020</span>
	            	</div>
	            	<div className="w100">
	            		<IconButton className="right" aria-label="more"><MoreVertIcon /></IconButton>
	            	</div>
	            </div>

	            <div className="actMsg">
	            	<h5 className="colorDiariko">
	            		Les questions les plus posées dans un entretien d'ambauche
	            	</h5>
	            	<p>
	            		1. Parlez-moi de vous?
						Soyez synthétique et clair, réponse en 30 secondes max. Recentrez la question sur votre parcours professionnel. 
						Commencez par vos études, votre premier poste, le second, etc. jusqu à votre situation d'aujourd'hui.
	            	</p>
	            	<p>
	            		2. Quels sont d;après vous vos deux principaux défauts ?
						Préparez à l'avance cette question, classique mais qui déstabilise celles et ceux qui ne 
						l'ont pas préparée. Soyez concis.
	            	</p>	
	            	<p>
	            		3. Qu'avez-vous fait depuis votre dernier emploi ?
						Il est important là, surtout si vous traversez une période de recherche d'emploi 
						assez longue, d'expliquez comment vous structurez vos journées. 
						Ce qui est important c'est de donner l'image de quelqu un qui en veut, 
						qui ne baisse pas les bras, qui est dynamique et organisé.
	            	</p>
	            	<p>
	            		4. Pourquoi avez-vous répondu à notre annonce ?
						Expliquez le lien avec vos études ou bien la progression professionnelle que 
						cela vous ferez faire (découverte de nouvelles fonctions, d'un nouveau secteur, 
						de nouvelles responsabilités, etc.). Expliquez aussi ce que vous pensez pouvoir apporter 
						à l'entreprise.
	            	</p>
	            </div>

	            <div className="actCoeur">
	            	<div className="flex w100">
		            	<div className="flex1">
			            	<IconButton aria-label="add to favorites"><FavoriteIcon /></IconButton>
					        	<span className="spanD">3 J'adore</span>
				        </div>
				        <div className="flexR">
				        	<span className="spanD right">0 Commentaires</span>
				        </div>
			        </div>
	            </div>   
	        </div>
	    </div>
    </div>
  );
}
