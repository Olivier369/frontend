import React from "react";
import { Link, withRouter } from "react-router-dom";
// @material-ui/core
//import { makeStyles } from "@material-ui/core/styles";
import "assets/css/diariko.css";
//import avatar from 'assets/img/sidebar-1.jpg';
import Couverture from "assets/img/profil/coverture2.jpg";

import axios from "axios";
//import Button from '@material-ui/core/Button';
 
//import Language from '@material-ui/icons/Language';

import MenuAct from "components/Diariko/Accueil/Menu";
//import routes from "routes.js";

import AuthService from "service/auth.service";
//const API_URL = "http://localhost:8080/api/";
const API_URL = "https://diariko.com/api/";

///
class Menu extends React.Component {

    constructor(props) {
        super(props);
        this.findOne = this.findOne.bind(this);
        this.state = {
          us: []
        };
    }

    componentDidMount() {
        const user = AuthService.getCurrentUser();

        if (!user) {
            console.log("User not found");
            const { history } = this.props;
            history.push("/adminConnection/connection");
        }
        else {
            console.log("User existe");
            this.findOne(user.id);
        }
        

    }

    findOne(id) {
        axios.get(API_URL + "auth/findOne/" + id)
            .then(user => {
                this.setState({
                    us: user.data,
                });
            });
    }

    
    render() {
        const { us } = this.state;

        return (
            <div>
                <div className="">
                    <div className="profilImg pt-15">
                        <div className="avatar flex-center imgProfil" style={{ backgroundImage: `url(${require("assets/img/profil/coverture2.jpg")})` }}>
                            
                        </div>
                        <div className="nomProfil">
                            <br/>
                            <Link to="/index/profil">
                            <p>
                                { us.prenom } { us.nom }
                            </p>
                            </Link>
                        </div>
                    </div>
                    <div className="bar">

                    </div>  
                	<div className="menu flex-center">
                        <MenuAct/>
                    </div>
                </div>
            </div>
        );
    }
}
export default withRouter(Menu);