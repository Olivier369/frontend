import React from "react";

import { Container } from 'reactstrap';
import "assets/css/diariko.css";

export default function Apropos() {
  return (
    <Container className="themed-container fix-cont" fluid="xl">
    	<div id="content">
	    	<div className="head">
	    		<p className="titre">Qui sommes nous?</p>
	    	</div>
	    	<div className="contenue">
	    		<p className="cont_p">
	    			Diariko est une application web et mobile. Diariko intervient principalement dans trois domaines à 
	    			savoir le domaine professionnel, éducatif et celui de la communication. 
	    			En couvrant ces domaines, Diariko aide ses utilisateurs à tisser un lien plus 
	    			fort avec leurs proches, leurs familles et leurs amis. Cette application les 
	    			accompagne dans leur cursus scolaire et facilite leur intégration au monde du travail.
	    		</p>
	    	</div>
    	</div>
    </Container>	
  );
}