import React from "react";

import homeimage from "assets/img/font/homeimage.jpg";

import { Container,  Col } from 'reactstrap';

import "assets/css/diariko.css";

export default function Abonner() {
  return (
  	<Container className="themed-container fix-cont" fluid={true} id="ins-act">
  		<div className="flex-center">
	    	<Col xs="12" sm="6" id="ins-img">
	    		<img src={homeimage} alt="diariko"id="homeimage" />
	    	</Col>
	    </div>
    </Container>
  );
}