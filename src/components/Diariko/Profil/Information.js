import React from 'react';
import Moment from 'react-moment';
import 'moment-timezone';

import "assets/css/center.scss";
import "assets/css/diariko.css";

import { Container } from 'reactstrap';
import axios from "axios";

import AuthService from "service/auth.service";
//const API_URL = "http://localhost:8080/api/";

const API_URL = "https://diariko.com/api/";

class Information extends React.Component {
	constructor(props){
		super(props);
		this.findUser = this.findUser.bind(this);
		this.findPays = this.findPays.bind(this);
		this.findRegion = this.findRegion.bind(this);
		this.findCommune = this.findCommune.bind(this);
        this.state = {
          us: [],
          pays: [],
          region: [],
          commune: [],
          userRole: []
        };
	}
	componentWillMount() {
		const user = AuthService.getCurrentUser();
		this.setState({
			userRole: user.roles
		});
		this.findUser(user.id);
		this.findPays(user.pays);
		this.findRegion(user.region);
		this.findCommune(user.commune);

	}
	findUser(id) {
        axios.get(API_URL + "auth/findRole/" + id)
            .then(user => {
                this.setState({
                    us: user.data,
                });
                console.log(user);
            });
    }
    findPays(id) {
    	axios.get(API_URL + "pays/" + id)
			.then(pays => {
				this.setState({
					pays: pays.data,
				})
			});
    }
    findRegion(id) {
    	axios.get(API_URL + "pays/re/" + id)
			.then(region => {
				this.setState({
					region: region.data,
				})
			});
    }
    findCommune(id) {
    	axios.get(API_URL + "pays/co/" + id)
			.then(commune => {
				this.setState({
					commune: commune.data,
				})
			});
    }
	render() {
		const { us, pays, region, commune, userRole } = this.state;

		
		return (
			<Container>
						<div className="boxShadow mb-10">
							<div className="">
								<div className="profilContent">
						        	<div>
						        		<p className="fs-12 colorDiariko">Personnels</p>
							          	<div className="information block">
							          		<div className="flex w100">
							          			<label htmlFor="nom" className="align lab150 fs-12">Nom</label><p className="align" id="nom">{us.nom}</p> 
							          		</div>
							          		<div className="flex w100">
							          			<label htmlFor="date" className="align lab150 fs-12">Prenom</label><p className="align" id="prenom">{us.prenom}</p> 
							          		</div>
							          		<div className="flex w100">
							          			<label htmlFor="date" className="align lab150 fs-12">Naissance</label>
							          			<p className="align" id="date">
							          				<Moment format="DD MM YYYY" date={us.dateNais}/>
							          			</p> 
							          		</div>
							          		<div className="flex w100">
							          			<label htmlFor="date" className="align lab150 fs-12">Adresse</label><p className="align" id="date">{us.adresse}</p> 
							          		</div>
							          		<div className="flex w100">
							          			<label htmlFor="date" className="align lab150 fs-12">Location</label><p className="align" id="date">{commune.nom}, {region.nom}, {pays.nom}</p> 
							          		</div>
							          		<div className="flex w100">
							          			<label htmlFor="date" className="align lab150 fs-12">Email</label><p className="align" id="prenom">{us.email}</p> 
							          		</div>
							          		<div className="flex w100">
							          			<label htmlFor="date" className="align lab150 fs-12">Username</label><p className="align" id="prenom">{us.username}</p> 
							          		</div>
						          		</div>
					          		</div>
					          		<br/>
					          		<div>
							            <p className="fs-12 colorDiariko">Privilege</p>
							          	<div className="information block">
							          		<p className="fs-12 ">Mon abonnement</p>
							          		<div className="flex w100">
							          			<label htmlFor="date" className="align lab150 fs-12">
							          			{ userRole.map(role => 
							          					role 
							          				)}
							          			</label>
							          		</div>
							          		<div className="flex w100">
							          			<label htmlFor="date" className="align lab150 fs-12">Date d'inscription</label><p className="align" id="date"><Moment format="DD MM YYYY - hh:mm" date={us.createdAt}/></p> 
							          		</div>
						          		</div>
					          		</div>
								</div>
							</div>
						</div>
			</Container>
		);
	}
}
export default Information;