import React, { Component } from 'react';
//import Moment from 'react-moment';
//import 'moment-timezone';

import "assets/css/center.scss";
import "assets/css/diariko.css";

import { Container, Row, Col, Input } from 'reactstrap';
import axios from "axios";
//import Form from "react-validation/build/form";
import Button from '@material-ui/core/Button';

import AuthService from "service/auth.service";
//const API_URL = "http://localhost:8080/api/";

const API_URL = "https://diariko.com/api/";

class Photo extends Component {
	constructor(props) {
		super(props);
		this.findImage = this.findImage.bind(this);
		this.handleImage = this.handleImage.bind(this);
		this.uploadImage = this.uploadImage.bind(this);

		this.state = {
			us: [],
			image: [],
			file: null,
			file_tmp: null,
			loading: false,
		}
	}

	componentWillMount() {
		const user = AuthService.getCurrentUser();
		this.setState({
			us: user,
		});
		console.log(user.id);
		this.findImage(user.id);
	}

	findImage(id) {
        axios.get(API_URL + "image/" + id)
            .then(image => {
                this.setState({
                    image: image.data,
                });
                console.log(image.data);
            })
            .catch(err => {
            	console.log(err);
            });
    }

    handleImage(e) {
    	this.setState({
    		file: e.target.files[0]
    	});
    }

    uploadImage(e) {
    	e.preventDefault();
    	const data = new FormData();
    	this.setState({
    		loading: true
    	});
    	data.append('file', this.state.file);
    	data.append('userId', this.state.us.id);

    	console.log(data);
    	return axios.post(API_URL + "image/", data, {
    		headers: {
    			"Content-Type" : "application/json"
    		}})
    		.then((res) => {
    			console.log(res);
    		})
    		.catch(err => {
    			console.log(err);
    		});

    }
    render() {
    	const { image } = this.state;
    	return (
    		<Container>
    				<Input type="file" onChange={this.handleImage}/>
    				<Button onClick={this.uploadImage}>Telecharger</Button>
    			<div className="avatar flex-center">
    				
                    <img src={ this.state.file } className="imgProfil" alt="avatar"/>
                </div>
                <Row>
                	<Col xs="6" sm="12">
                			<img src={image.file} className="imgProfil" alt="imgp"/>
                	</Col>
                </Row>
    		</Container>
    	);
    }
}
export default Photo;