import React from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap';
import classnames from 'classnames';

import Information from "./Information";
import Sante from "./Sante.js";
//import Photo from "./Photo";

import 'assets/css/diariko.css';
import 'assets/css/center.scss';

class Profil extends React.Component {
	constructor(props) {
		super(props);
		this.handleTab = this.handleTab.bind(this);

		this.state = {
			activeTab: 1
		}
	}
	componentWillMount() {
		this.handleTab(1);
	}

	handleTab(tab) {
		this.setState({
			activeTab: tab
		});
	}

	render() {
		const { activeTab } = this.state;
		return (
			<div>
			<div>
		      <Nav tabs>
		        <NavItem>
		          <NavLink className={classnames({ active: activeTab ===1 })} onClick={() => { this.handleTab('1'); }} >
		            Mes informations
		          </NavLink>
		        </NavItem>
		        <NavItem>
		          <NavLink className={classnames({ active: activeTab === '2' })} onClick={() => { this.handleTab('2'); }} >
		            Ma sante
		          </NavLink>
		        </NavItem>
		      </Nav>
		      <TabContent activeTab={activeTab}>
		      	<TabPane tabId="1">
		          <Information/>
		        </TabPane>
		        <TabPane tabId="2">
		        	<Sante/>
		        </TabPane>
		      </TabContent>
		    </div>
		    </div>
		);
	}
}
export default Profil;