import React from "react";
import { Link } from "react-router-dom";

import "assets/css/diariko.css";
import "assets/css/center.scss";

export default function Famille (){
	return (
		<div className="mt-100">
			<div className="boxShadow mb-10">
				<div className="center">
					<p>Bientot sur <Link to="/index/actualite" className="colorDiariko">DIARIKO</Link></p>
					<p id="fs-13">En cour de developpement</p>
				</div>
			</div>
		</div>	
	);
}