import React from "react";
// @material-ui/core
import { makeStyles } from "@material-ui/core/styles";
// @material-ui/icons
import { Form, FormGroup, Input, FormText, Row, Col, InputGroup, InputGroupAddon, InputGroupText } from 'reactstrap';
// core components
import Side from "assets/img/sidebar-1.jpg";
import Search from '@material-ui/icons/Search';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';

import "assets/css/diariko.css";

const useStyles = makeStyles((theme) => ({
  button: {
    margin: theme.spacing(1),
  },
}));

export default function Config() {
    const classes = useStyles();
  return (
    	<div className="flex">
            <div className="cProfil">
                <div className="cpAvatar">
                    <img src={Side} alt="" id="imgRonde"/>
                </div>
                <div ClassName="cpContenu"></div>
            </div>
            <div className="cConfig">

                <p></p>
            </div>
            <div>
            </div>
        </div>
  );
}
