// Section pour afficher les pubs, promotion, nouveautes de l'admin
// 
import React from "react";
import { Link } from "react-router-dom";
// @material-ui/core

import  IconDon  from "assets/img/FA/people-carry.svg";

// core components
import Don from "assets/img/DON.jpg";

import "assets/css/diariko.css";
import "assets/css/center.scss";
console.log({IconDon});

export default function Publicite() {
	return (
		<div>
			<div className="boxShadow">
				<div className="imgDonnation">
					<img src={ Don } className="w100" alt="don"/>
				</div>
				<div className="msgDon">
					<p>Supporter-nous en faisons un <Link to="#">don</Link></p>
				</div>
				<div className="flex-center">
					<div className="btnDon">
						<img src={ IconDon } alt="donation"/>
						<span>DON</span>
					</div>
				</div>
				
			</div>
		</div>
	);
}