import React from "react";
import { Link } from "react-router-dom";
// @material-ui/core
//import { makeStyles } from "@material-ui/core/styles";
import "assets/css/diariko.css";

// @material-ui/icons
import Button from '@material-ui/core/Button';
 
import Language from '@material-ui/icons/Language';
import Money from '@material-ui/icons/LocalHospital';
import Event from '@material-ui/icons/EventAvailable';
import Conseil from '@material-ui/icons/WorkOutline';
import Work from '@material-ui/icons/BusinessCenter';
import Famille from '@material-ui/icons/PeopleOutline';


class Menu extends React.Component {

    render() {
        return (
            <ul id="menuSimple" className="pl-0">
                <li> 
                    <Link to="/index/actualite">
                        <Button variant="contained" color="primary"  size="medium" id="btnSimple" startIcon={<Language/>}>Actualite</Button>
                    </Link>
                </li>
                <li> 
                    <Link to="/index/analyse">
                        <Button variant="contained" color="primary"  size="medium" id="btnSimple" startIcon={<Money/>}>Diagnostic</Button>
                    </Link>
                </li>
                <li> 
                    <Link to="/index/famille">
                        <Button variant="contained" color="primary"  size="medium" id="btnSimple" startIcon={<Famille/>}>Famille</Button>
                    </Link>
                </li>
                
                <li>
                    <Link to="/index/conseil">
                    <Button variant="contained" color="primary"  size="medium"  id="btnSimple" startIcon={<Conseil/>}>Conseil</Button>
                    </Link>
                </li>
                <li>
                    <Link to="/index/events">
                    <Button variant="contained" color="primary"  size="medium"  id="btnSimple" startIcon={<Event/>}>Events</Button>
                    </Link>
                </li>
                <li>
                    <Link to="/index/job">
                    <Button variant="contained" color="primary"  size="medium"  id="btnSimple" startIcon={<Work/>}>Job</Button>
                    </Link>
                </li>
            </ul>
        );
    }
}
export default Menu;