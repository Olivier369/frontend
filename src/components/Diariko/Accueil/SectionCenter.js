// Section center, actualite,...
import React from "react";
// @material-ui/core
//import { makeStyles } from "@material-ui/core/styles";

import Avatar from '@material-ui/core/Avatar';
import Moment from 'react-moment';

import MoreVertIcon from '@material-ui/icons/MoreVert';
import IconButton from '@material-ui/core/IconButton';
import FavoriteIcon from '@material-ui/icons/Favorite';
//import Send from '@material-ui/icons/Send';
import Button from '@material-ui/core/Button';

// core components
import Couverture from "assets/img/profil/coverture2.jpg";

import TextField from '@material-ui/core/TextField';
import "assets/css/diariko.css";
import "assets/css/center.scss";
import axios from "axios";

import AuthService from "service/auth.service";
//const API_URL = "http://localhost:8080/api/";

const API_URL = "https://diariko.com/api/";



class Center extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			us: [],
			actualite: [],
			act: "",
			loading: false
		}
		this.findAll = this.findAll.bind(this);
		this.findUser = this.findUser.bind(this);
		this.postActualite = this.postActualite.bind(this);
		this.handleActualite = this.handleActualite.bind(this);
	}

	componentWillMount() {
		const user = AuthService.getCurrentUser();
		this.findUser(user.id);
		this.findAll();
	}

	handleActualite(e) {
		this.setState({
			act: e.target.value
		});
		console.log(this.state.act);
	}

	postActualite(e) {
		e.preventDefault();
		const id = this.state.us.id;
		const nom = this.state.us.prenom + " " + this.state.us.nom;
		const act = {
			titre: nom,
			contenu: this.state.act,
			userId: id,

		};
		this.setState({
			loading: true
		});

		return axios.post(API_URL + "actualite", act, {
	        headers: {
	            'Content-Type': 'application/javascript;charset=utf-8'
	        }})
			.then((res) => {
				this.findAll();
				console.log(res.data);
				this.setState({
					act: "",
					loading: false
				});
			})
			.catch((error) => {
				const resMessage = (
					error.response &&
					error.response.data &&
					error.response.data.message) ||
					error.message ||
					error.toString();

				console.log(resMessage);
			});
	}
	findAll() {
		axios.get(API_URL + "actualite")
			.then(act => {
				this.setState({
					actualite: act.data
				});
				
				console.log(act.data);
				
				
			})
			.catch(err => {
				console.log(err);
			});
	}
	findUser(id) {
        axios.get(API_URL + "auth/findRole/" + id)
            .then(user => {
                this.setState({
                    us: user.data,
                });
            });
    }
	render() {
		  const { actualite, act } = this.state;

		  return (
		  	<div>
		  		

		  		<div className="boxShadow mb-10 fixed" id="paddAuto">
			    	<div className="sectionCenter">
			            <div className="actTete flex w100">
			            	<div className="pdpRonde">
			            		<Avatar alt="Travis Howard" src={ Couverture } />
			            	</div>
			            	<div className="w100 ml-10">
			            		<TextField
							          id="filled-textareas"
							          label="Publier vos exploit..."
							          placeholder=""
							          multiline
							          variant="filled"
							          className="w100 field"
							          onChange={ this.handleActualite }
							          value={ act }
							        />
							        
			            	</div>
			            	<Button id="btnSimple" onClick={this.postActualite}>
			            		{this.state.loading && (
						     		<span className="spinner-border spinner-border-sm"></span>
						     	)}
			            		<span> Poste</span>
			            	</Button>
			            </div>
			        </div>
			    </div>
			    { actualite.map((act =>
			  	<div key={ act.id }className="boxShadow mb-10">
			    	<div className="sectionCenter">
			            <div className="actTete flex w100">
			            	<div className="pdpRonde">
			            		<Avatar alt="Travis Howard" src={ Couverture } />
			            	</div>
			            	<div className="nomPdp nom50">
			            		<span className="colorDiariko spanN">{ act.titre }</span>
			            		<br/>
			            		<span className="spanD">
			            			<Moment format="DD-MM-YYYY, hh:mm" date={act.createdAt}/>
			            		</span>
			            	</div>
			            	<div className="w100">
			            		<IconButton className="right" aria-label="more"><MoreVertIcon /></IconButton>
			            	</div>
			            </div>
			            <div className="actMsg">
			            	<p>
			            		{ act.contenu }
			            	</p>
			            </div>
			            <div className="w100 flex-center">
			            <div className="actPhoto flex">
			            </div>
			            </div>
			            <div className="actCoeur">
			            	<div className="flex w100">
				            	<div className="flex1">
					            	<IconButton aria-label="add to favorites"><FavoriteIcon /></IconButton>
							        	<span className="spanD">0 J'adore</span>
						        </div>
						        <div className="flexR">
						        	<span className="spanD right">0 Commentaires</span>
						        </div>
					        </div>
			            </div>
			        </div>
			    </div>
			    )) }
		    </div>
		  );
  		}
}
export default Center;