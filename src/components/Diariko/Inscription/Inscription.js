import React from "react";
import { Link, withRouter } from "react-router-dom";
//import PropTypes from 'prop-types';

import axios from "axios";

//import profil from "assets/img/avatar/profil.jpg";
//import fondBleu from "assets/img/font/fontBleu.png";

import {  Input, FormGroup, Label } from 'reactstrap';


import Form from "react-validation/build/form";
import CheckButton from "react-validation/build/button";

import "assets/css/diariko.css";

import Button from '@material-ui/core/Button';
//import SaveIcon from '@material-ui/icons/Save';
import Save from '@material-ui/icons/SaveAlt';
import Restore from '@material-ui/icons/Restore';


//stepper
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';


//import { makeStyles, withStyles, useTheme } from "@material-ui/core/styles";

//import PaysService from "service/pays.service";

//const API_URL = "http://localhost:8080/api/pays";
//const API_URL_POST = "http://localhost:8080/api/auth/signup";

const API_URL = "https://diariko.com/api/pays";
const API_URL_POST = "https://diariko.com/api/auth/signup";

const required = value => {
	if (!value) {
		return (
			<span className="alertRouge">Cette champ est obligatoire!</span>
		);
	}
};///

class Inscription extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			nom: "" ,
			prenom: "",
			dateNais: "",
			sexe: "Male",
			adresse: "",  
			email: "",  
			password: "", 
			username: "",  
			pays: "",  
			region: "",  
			commune: "",  
			tel: "", 
			activeStep: 0,
			loading: false,
			message: "",
			pays: [],
			region: [],
			commune: [],
			paysId: 0,
			regionId: 0,
			communeId:0,
			erNom: "",
			erPrenom: "",
			erDateNais: "",
			erPays: "",
			erRegion: "",
			erCommune: "",
			erUsername: "",
			erPassword: "",
			erEmail: "",
			err: "Cette champ est obligatoire"
		};
		this.handleNext1 = this.handleNext1.bind(this);
		this.handleNext2 = this.handleNext2.bind(this);
		this.handleNext3 = this.handleNext3.bind(this);
		this.handlePrev1 = this.handlePrev1.bind(this);
		this.handlePrev2 = this.handlePrev2.bind(this);
		this.handleReset = this.handleReset.bind(this);

		this.getRegion = this.getRegion.bind(this);
		this.getCommune = this.getCommune.bind(this);

		this.getNom = this.getNom.bind(this);
		this.getPrenom = this.getPrenom.bind(this);
		this.getDateNais = this.getDateNais.bind(this);
		this.getSexe = this.getSexe.bind(this);
		this.getEmail = this.getEmail.bind(this);
		this.getPassword = this.getPassword.bind(this);
		this.getUsername = this.getUsername.bind(this);
		this.getTel = this.getTel.bind(this);
		this.getCommuneId = this.getCommuneId.bind(this);
		this.getAdresse = this.getAdresse.bind(this);

		this.signUp = this.signUp.bind(this);
		
	}

	componentWillMount() {
		this.getPays();
	}
	handleNext1(){
		if(this.state.nom.length === 0 ){
			this.setState({
				erNom: this.state.err
			});
		} else {
			this.setState({
				erNom: ""
			});
		}
		if(this.state.prenom.length === 0 ){
			this.setState({
				erPrenom: this.state.err
			});
		} else {
			this.setState({
				erPrenom: ""
			});
		}
		if(this.state.dateNais.length === 0 ){
			this.setState({
				erDateNais: this.state.err
			});
		} else {
			this.setState({
				erDateNais: ""
			});
		}

		if(this.state.nom.length !== 0 && this.state.prenom.length !== 0 && this.state.dateNais.length !== 0) {
			this.setState({
				activeStep: 1,
				erNom: "",
				erPrenom: "",
				erDateNais: "",
			});
		}
		
	}
	handleNext2(){
		if(this.state.pays.length === 0 ){
			this.setState({
				erPays: this.state.err
			});
		} else {
			this.setState({
				erPays: ""
			});
		}
		if(this.state.region.length === 0 ){
			this.setState({
				erRegion: this.state.err
			});
		} else {
			this.setState({
				erRegion: ""
			});
		}
		if(this.state.commune.length === 0 ){
			this.setState({
				erCommune: this.state.err
			});
		} else {
			this.setState({
				erCommune: ""
			});
		}
		if(this.state.pays.length !== 0 && this.state.region.length !== 0 && this.state.commune.length !== 0) {
			this.setState({
				activeStep: 2,
				erPays: "",
				erRegion: "",
				erCommune: "",
			});
		}
	}
	handleNext3(){
		if(this.state.username.length < 4 ){
			this.setState({
				erUsername: "Username doit contenir au moin 4 caractere"
			});
		} else {
			this.setState({
				erUsername: ""
			});
		}
		if(this.state.email.length === 0 ){
			this.setState({
				erEmail: this.state.err
			});
		} else {
			this.setState({
				erEmail: ""
			});
		}
		if(this.state.password.length < 8 ){
			this.setState({
				erPassword: "Le mot de pass doit contenir au moin 8 caractere"
			});
		} else {
			this.setState({
				erPassword: ""
			});
		}
		if(this.state.username.length !== 0 && this.state.email.length !== 0 && this.state.password.length > 7) {
			this.setState({
				activeStep: 3,
				erUsername: "",
				erEmail: "",
				erPassword: "",
			});
		}
	}
	handlePrev2() {
		this.setState({
			activeStep: 1
		});
	}
	handlePrev1() {
		this.setState({
			activeStep: 0
		});
	}
	handleReset() {
		this.setState({
			activeStep: 0,
			message: ""
		});
	}
	getPays() {
		axios.get(API_URL + "")
			.then(response => {
				this.setState({
					pays: response.data
				});
			});
	}
	getRegion(e) {
		axios.get(API_URL + "/region/" + e.target.value)
			.then(response => {
				this.setState({
					region: response.data
				});
			});
		this.setState({
			paysId: e.target.value
		})
	}
	getCommune(e) {
		axios.get(API_URL + "/commune/" + e.target.value)
			.then(response => {
				this.setState({
					commune: response.data,
				});
			});
		this.setState({
			regionId: e.target.value
		})
	}
	getNom(e) {
		this.setState({
			nom: e.target.value
		});
	}
	getPrenom(e) {
		this.setState({
			prenom: e.target.value
		});
	}
	getSexe(e) {
		this.setState({
			sexe: e.target.value
		});
	}
	getDateNais(e) {
		this.setState({
			dateNais: e.target.value
		});
	}
	getAdresse(e) {
		this.setState({
			adresse: e.target.value
		});
	}
	getEmail(e) {
		this.setState({
			email: e.target.value
		});
	}
	getPassword(e) {
		this.setState({
			password: e.target.value
		});
	}
	getUsername(e) {
		this.setState({
			username: e.target.value
		});
	}
	getTel(e) {
		this.setState({
			tel: e.target.value
		});
	}
	getCommuneId(e) {
		this.setState({
			communeId: e.target.value
		});
	}

	signUp(e) {
		e.preventDefault();

		this.setState({
			message: "",
			loading: true
		});

		this.form.validateAll();

		const user = {
			nom:this.state.nom ,
			prenom: this.state.prenom,
			dateNais: this.state.dateNais,
			sexe: this.state.sexe,
			adresse: this.state.adresse,  
			email: this.state.email,  
			password: this.state.password, 
			username: this.state.username,  
			pays: this.state.paysId,  
			region: this.state.regionId,  
			commune: this.state.communeId,  
			tel: this.state.tel, 
		};
		console.log(user);
		
		return axios.post(API_URL_POST, user, {
	        headers: {
	            'Content-Type': 'application/json'
	        }})
			.then((res) => {
				const { history } = this.props;
				history.push("/admin/confirmation");
				console.log(res.data);
			})
			.catch((error) => {
				const resMessage = (
					error.response &&
					error.response.data &&
					error.response.data.message) ||
					error.message ||
					error.toString();

				this.setState({
					loading: false,
					message: resMessage
				});
			});
		
		
	}
	render() {
		let pays = this.state.pays;
		let regions = this.state.region;
		let communes = this.state.commune;
    return (
	    <div className="formInsc">
	    	<Form 
	    		onSubmit={this.signUp}
				ref={c => {
					this.form = c;
				}}
	    	>
	    		<Stepper activeStep={this.state.activeStep} orientation="vertical">
	    			<Step key={0}>
			            <StepLabel>Information personnel</StepLabel>
			            <StepContent>
			            	<Typography component={'span'}>
				    			<FormGroup className="flex" id="formFlex">
							        <Label for="nom" className="fs-13 labelInc">Nom</Label>
							        <div className="block">
							        	<Input type="text" validations={[required]} name="nom" id="w-200" value={this.state.nom} onChange={this.getNom} placeholder="Votre nom" />
							        	<div className="alertRouge">{this.state.erNom}</div>
							        </div>
							    </FormGroup>
							    <FormGroup className="flex" id="formFlex">
							        <Label for="prenom" className="fs-13 labelInc">Prenom</Label>
							        <div className="block">
							        	<Input type="text" validations={[required]} name="prenom" value={this.state.prenom} onChange={this.getPrenom} id="w-200" placeholder="Votre prenom" />
							    		<div className="alertRouge">{this.state.erPrenom}</div>
							    	</div>
							    </FormGroup>
							    <FormGroup className="flex" id="formFlex">
							        <Label for="nom" className="fs-13 labelInc">Date de naissance</Label>
							        <div className="block">
							        	<Input type="date" validations={[required]} name="date_nais" value={this.state.dateNais} onChange={this.getDateNais} id="w-200" placeholder="" />
							    		<div className="alertRouge">{this.state.erDateNais}</div>
							    	</div>
							    </FormGroup>
							    <FormGroup className="flex" id="formFlex">
							        <Label for="sexe" className="fs-13 labelInc">Sexe</Label>
							        <Input type="select" validations={[required]} name="sexe"  onChange={this.getSexe} id="w-200">
							        	<option key="Male" value="Male">Male</option>
							          	<option key="Femelle" value="Femelle">Femelle</option>
							        </Input>
							    </FormGroup>
					    	</Typography>
					    	<div className="">
				                <div>
				                  <Button variant="outlined" onClick={this.handleNext1} color="primary">
				                    Suivant
				                  </Button>
				                </div>
				            </div>
				    	</StepContent>
					</Step>
					<Step key={1}>
			            <StepLabel>Coordonnee</StepLabel>
			            <StepContent>
			            	<Typography component={'span'}>
				    			<FormGroup className="flex" id="formFlex">
							        <Label for="pays" className="fs-13 labelInc">Pays</Label>
							        <div className="block">
								        <Input type="select" validations={[required]} onChange={this.getRegion} name="pays" id="w-200">
								        	{ 
								        		pays.map((p => 
								        			<option key={ p.id } value={p.id}>{p.nom}</option>
								        		))
								        	} 	
								        </Input>
								        <div className="alertRouge">{this.state.erPays}</div>
							        </div>
							    </FormGroup>
							    <br/>
							    <FormGroup className="flex" id="formFlex">
							        <Label for="region" className="fs-13 labelInc">Region</Label>
							        <div className="block">
								        <Input type="select" validations={[required]} onChange={this.getCommune} name="region" id="w-200">
								        	{ 
								        		regions.map((r => 
								        			<option key={ r.id } value={r.id}>{r.nom}</option>
								        		))
								        	}
								        </Input>
								        <div className="alertRouge">{this.state.erRegion}</div>
							        </div>
							    </FormGroup>
							    
							    <FormGroup className="flex" id="formFlex">
							        <Label for="commune" className="fs-13 labelInc">Commune</Label>
							        <div className="block">
								        <Input type="select"  onChange={this.getCommuneId} name="commune" id="w-200">
								        	{ 
								        		communes.map((c => 
								        			<option key={ c.id } value={c.id}>{c.nom}</option>
								        		))
								        	}
								        </Input>
								        <div className="alertRouge">{this.state.erCommune}</div>
							        </div>
							    </FormGroup>
							    <FormGroup className="flex" id="formFlex">
							        <Label for="adresse" className="fs-13 labelInc">Adresse</Label>
							        <Input type="text" validations={[required]} name="adresse" onChange={this.getAdresse}  value={this.state.adresse} id="w-200" placeholder="Rue n:..." />
							    </FormGroup>
							</Typography>
							<div className="">
				                <div>
				                  <Button disabled={this.state.activeStep === 0} onClick={this.handlePrev1}>Retour</Button>
				                  
				                  <Button variant="outlined" color="primary" onClick={this.handleNext2}>
				                    Suivant
				                  </Button>
				                </div>
				            </div>
				    	</StepContent>
					</Step>
					<Step key={2}>
			            <StepLabel>Authentification</StepLabel>
			            <StepContent>
			            	<Typography component={'span'}>
			            		<FormGroup className="flex" id="formFlex">
							        <Label for="username" className="fs-13 labelInc">Username</Label>
							        <div className="block">
							        	<Input type="text" validations={[required]} name="username" id="w-200" onChange={this.getUsername} value={this.state.username} placeholder="" />
							    		<div className="alertRouge">{this.state.erUsername}</div>
							    	</div>
							    </FormGroup>
							    <FormGroup className="flex" id="formFlex">
							        <Label for="tel" className="fs-13 labelInc">Telephone</Label>
							        <Input type="text" validations={[required]} name="tel" id="w-200" onChange={this.getTel} value={this.state.tel} placeholder="" />
							    </FormGroup>
				    			<FormGroup className="flex" id="formFlex">
							        <Label for="email" className="fs-13 labelInc">Email</Label>
							        <div className="block">
							        	<Input type="email" validations={[required]} name="email" id="w-200" onChange={this.getEmail} value={this.state.email}placeholder="" />
							    		<div className="alertRouge">{this.state.erEmail}</div>
							    	</div>
							    </FormGroup>
							    <FormGroup className="flex" id="formFlex">
							        <Label for="password" className="fs-13 labelInc">Mot de pass</Label>
							        <div className="block">
							        	<Input type="password" validations={[required]} name="password" id="w-200" onChange={this.getPassword} value={this.state.password}id="password" placeholder="" />
							    		<div className="alertRouge">{this.state.erPassword}</div>
							    	</div>
							    </FormGroup>
				    		</Typography>
				    		<div className="actionsContainer">
				                <div>
				                  <Button disabled={this.state.activeStep === 1} onClick={this.handlePrev2} >Retour</Button>
				                  <Button variant="outlined" color="primary" onClick={this.handleNext3}>
				                    Terminer
				                  </Button>
				                </div>
				            </div>
				    	</StepContent>
					</Step>					    
				</Stepper>
				{this.state.activeStep === 3 && (
			        <Paper square elevation={0} className="">
			          <p id="fs-13">
			          	{this.state.message && (
				      		<div className="form-group">
				      			<span className="alertRouge">
				      				{this.state.message}
				      			</span>
				      		</div>
				      	)}
			          </p>
			          <br/>
			          <Button onClick={this.handleReset} startIcon={<Restore/>}>
			            Modifier
			          </Button>
			          <Button startIcon={<Save/>} variant="outlined" type="submit" color="primary">
			            {this.state.loading && (
				     		<span className="spinner-border spinner-border-sm"></span>
				     	)}
			            <span> Enregistrer</span>
			          </Button>
				      	<CheckButton
				      		style={{ display: "none" }}
				      		ref={c => {
				      			this.checkBtn = c;
				      		}}
				      	/>
			        </Paper>
			    )}
		    </Form>
	    </div>
    );
}
}

export default withRouter(Inscription);