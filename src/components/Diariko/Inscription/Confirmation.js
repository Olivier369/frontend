import React from "react";
import { Link } from "react-router-dom";

import { Container, Row, Col } from 'reactstrap';

import "assets/css/diariko.css";

import Button from '@material-ui/core/Button';

class Confirmation extends React.Component {

  render() {

  return (
  	<Container className="themed-container fix-cont" fluid={true} id="container-section">
  		<Row>
  			<Col xs="6" sm="8">
			    <div className="message">
			    	<div className="title">
				       <h3>Confirmation</h3>
				    </div>
			    	<div className="msg-conf msg">
				    	<p className="fs-13"><strong className="colorDiariko">Diariko</strong> vous remercie pour votre inscription sur notre plateforme</p>
				    	<p className="fs-13">Un email a ete envoye a votre adresse pour la <strong className="colorDiariko">confirmation</strong> de votre compte</p>
				    	<p className="fs-13">Veuillez cliquer sur le lien decrit dans ce mail</p>
			    	</div>
			    </div>
			    <div className="msg-appl">
		    		<div className="title">
				       <h3>Nouveauté ICI</h3>
				    </div>
				    <div className="msg">
		    			<p className="fs-13">Telecharger <a href="#download">ici</a> l'application <strong className="colorDiariko">DIARIKO </strong>
		    			ou directement sur google play</p>
		    		</div>
		    	</div>
	    	</Col>
	    	<Col xs="12" sm="4">
	    		<div className="grosBtn">
	    			<Link to="/index/diariko">
				      	<Button
					        variant="outlined"
					        color="succes"
					        id="largeBtn"
					      >
					        Diariko
				      </Button>
				    </Link>
	    		</div>
	    		<div className="grosBtn">
	    			<Link to="/admin/contacter">
				      	<Button
					        variant="outlined"
					        color="primary"
					        id="largeBtn"
					      >
					        Faire un don
				      </Button>
				    </Link>
	    		</div>
	    		<div className="grosBtn">
	    			<Link to="/">
				      	<Button
					        variant="outlined"
					        color="secondary"
					        id="largeBtn"
					      >
					        Contacter nous
				      </Button>
				    </Link>
	    		</div>
	    		
	    	</Col>
	    </Row>
    </Container>
  );
}
}

export default Confirmation;