import React from "react";
import { withRouter } from "react-router-dom";

import logoDiariko from "assets/img/font/logokely.png";

import { Container, FormGroup, Label, Row, Col } from 'reactstrap';

import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";

import AuthService from "service/auth.service";

import "assets/css/diariko.css";

import Button from '@material-ui/core/Button';





const required = value => {
	if (!value) {
		return (
			<div className="alertRouge">
				Cette champ est obligatoire!
			</div>
		);
	}
};



	
///
class Connection extends React.Component {

	

	constructor(props) {
		super(props);
		this.handleLogin = this.handleLogin.bind(this);
		this.onChangeUsername = this.onChangeUsername.bind(this);
		this.onChangePassword = this.onChangePassword.bind(this);

		this.state = {
			username: "",
			password: "",
			loading: false,
			message: ""
			
		};
		
	}



	onChangeUsername(e) {
		this.setState({
			username: e.target.value
		});
	}

	onChangePassword(e) {
		this.setState({
			password: e.target.value
		});
	}

	routeChange() {
    	
	}

	handleLogin(e) {
		e.preventDefault();

		this.setState({
			message: "",
			loading: true
		});

		this.form.validateAll();

		if (this.checkBtn.context._errors.length === 0 ) {
			AuthService.login(this.state.username, this.state.password).then(
				() => {
					const { history } = this.props;
					history.push("/index/actualite");
					//this.routeChange();
					

					console.log("connected");
					//window.location.reload();
				},
				error => {
					const resMessage = (
						error.response &&
						error.response.data &&
						error.response.data.message) ||
						error.message ||
						error.toString();

					this.setState({
						loading: false,
						message: resMessage
					});
				}
			);
		}
	}

  render() {

  return (
  	<Container className="themed-container fix-cont" fluid={true} id="container-sect">
  		<Row id="rowConnection">
  			<Col xs="6" sm="12">
			    <div className="formInsc">
			    		<Row>
				    		<Col xs="12" sm="6">
						     	<div className="flex">
					    			<div id="imgLogoKely">
					    				<img src={logoDiariko} alt="lDiariko"/>
					    			</div>
					    			<span className="iariko">IARIKO</span>
					    		</div>
						    </Col>
			    			<Col xs="12" sm="6">
			    				<div className="block1">
				    				<div className="block2">
				    					<Form
				    						onSubmit={this.handleLogin}
				    						ref={c => {
				    							this.form = c;
				    						}}
				    					>
								    		<div className="formBlock">
								    			<div id="">
								    			  <FormGroup id="inputMin">
											        <Label for="email" className="labelForm">Username</Label>
											        <Input 
											        	type="text" 
											        	name="username" 
											        	id="email" 
											        	value={this.state.username}
											        	onChange={this.onChangeUsername}
											        	validations={[required]}
											        	placeholder="Votre username..." 
											        />
											      </FormGroup>
											      <FormGroup id="inputMin">
											        <Label for="password" className="labelForm">Mot de pass</Label>
											        <Input 
											        	type="password" 
											        	name="password" 
											        	id="password"
											        	value={this.state.password}
											        	onChange={this.onChangePassword}
											        	validations={[required]}
											        	placeholder="........"
											        />
											      </FormGroup>
								    			</div>
								    		</div>
									      	<Button
										        variant="contained"
										        color="primary"
										        size="medium"
										        disabled={this.state.loading}
										        type="submit"
										     >
										     	{this.state.loading && (
										     		<span className="spinner-border spinner-border-sm"></span>
										     	)}
										        <span> Se connecter</span>
									      	</Button>

									      	<p id="fs-13">
									          	{this.state.message && (
										      		<div className="form-group">
										      			<span className="alertRouge">
										      				{this.state.message}
										      			</span>
										      		</div>
										      	)}
									          </p>
									      	<CheckButton
									      		style={{ display: "none" }}
									      		ref={c => {
									      			this.checkBtn = c;
									      		}}
									      	/>
									    </Form>
								    </div>
							    </div>
						    </Col>
				      </Row>
				      <Row>
				      	<Col xs="12" sm="12">
				      		<div className="slogan">
					    			<p><strong className="iariko">Diariko, </strong>La reference du social web working, developpement, creativite, innovation. La communaute qui vous donne des opportunites pour faire des dons.</p>
					    		</div>
				      	</Col>
				      </Row>
			    </div>
	    	</Col>
	    </Row>
    </Container>
  );
}
}
export default withRouter(Connection);