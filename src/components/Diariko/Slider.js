import React from "react";
//import { Link } from "react-router-dom"

import fond from "assets/img/font/slider.jpg";

import { makeStyles } from "@material-ui/core/styles";
import "assets/css/diariko.css";

import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';

// dialog component
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Paper from '@material-ui/core/Paper';
import Draggable from 'react-draggable';

//
import Inscription from "./Inscription/Inscription.js";

function PaperComponent(props) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}
////

const useStyles = makeStyles((theme) => ({
  margin: {
    margin: theme.spacing(1),
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));



console.log(fond);

export default function Slider() {
  const classes = useStyles();

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
      setOpen(true);
    };

  const handleClose = () => {
      setOpen(false);
    };

  return (
    <div className="slider" style={{ backgroundImage: `url(${require("assets/img/font/slider.jpg")})` }}>
      <div className="opacity">
      </div>
        <Container id="slider-content">
        	<div className="sld-info">
        		<ul>
        			<li>Prenez soin de vous,</li>
        			<li>restez connecter avec vos proches et faites des échanges professionnels.</li>
        		</ul>
        		
        	</div>
        	<div className="btn-auth">
    				<Button variant="outlined" id="btnDiariko" size="large" color="primary" className={classes.margin} onClick={ handleClickOpen }>
    				  S'inscrire
    				</Button>
        	</div>
          <Dialog open={open} PaperComponent={PaperComponent} aria-labelledby="draggable-dialog-title" id="dialogForm">
              <DialogTitle className="colorDiariko" style={{ cursor: 'move' }} id="draggable-dialog-title">Inscrivez-vous</DialogTitle>
              <DialogContent>
                <Inscription/>
              </DialogContent>
              <DialogActions>
                  <Button onClick={handleClose} id="btnDiariko" color="primary">
                    Annuler
                </Button>
              </DialogActions>
          </Dialog>
        </Container>
      
    </div>
  );
}