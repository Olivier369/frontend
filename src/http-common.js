import axios from "axios";

export default axios.create({
	baseUrl: "https://diariko.com",
	headers: {
		"Content-type" : "application/json"
	}
});