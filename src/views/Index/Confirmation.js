import React from "react";

import ConfirmationComponent from "components/Diariko/Inscription/Confirmation.js";

export default function Confirmation() {
  return (
    <div id="container">
      <ConfirmationComponent/>
    </div>
  );
}
