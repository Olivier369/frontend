import React from "react";

import ConnectionComponent from "components/Diariko/Inscription/Connection.js";

export default function Connection() {
  return (
    <div id="container">
      <ConnectionComponent/>
    </div>
  );
}
