import React from "react";

import InscriptionComponent from "components/Diariko/Inscription/Inscription.js";

export default function Inscription() {
  return (
    <div id="container">
      <InscriptionComponent/>
    </div>
  );
}
