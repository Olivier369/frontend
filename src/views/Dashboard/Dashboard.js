import React from "react";

import Slider from "components/Diariko/Slider.js";
import Apropos from "components/Diariko/Apropos.js";
import Activite from "components/Diariko/Activite.js";
import Abonner from "components/Diariko/Abonner.js";

export default function Dashboard() {
  return (
    <div id="container">
      <Slider/>
      <Apropos/>
      <Activite/>
      <Abonner/>
    </div>
  );
}
